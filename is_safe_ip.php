<?php
/**
 * 安全IP检测，支持IP段检测
 * @param string $ip 要检测的IP
 * @param string|array $ips  白名单IP或者黑名单IP
 * @return boolean true 在白名单或者黑名单中，否则不在
 */
function is_safe_ip($ip="",$ips=""){
    if(!$ip) $ip = get_client_ip();  //获取客户端IP
    if($ips){
        if(is_string($ips)){ //ip用"," 例如白名单IP：192.168.1.13,123.23.23.44,193.134.*.*
            $ips = explode(",", $ips);
        }
    }else{ //读取后台配置 白名单IP
        $obj = new Setting();
        $ips = explode(",", $obj->getConfig("whiteip"));  
    }
    if(in_array($ip, $ips)){
        return true;
    }
    $ipregexp = implode('|', str_replace( array('*','.'), array('\d+','\.') ,$ips));  
    $rs = preg_match("/^(".$ipregexp.")$/", $ip);  
    if($rs) return true;
    return ;
}


/**
 * 获取客户端IP地址
 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
 * @param boolean $adv 是否进行高级模式获取（有可能被伪装）
 * @return mixed
 */
function get_client_ip($type = 0,$adv=false){
    $type       =  $type ? 1 : 0;
    static $ip  =   NULL;
    if ($ip !== NULL) return $ip[$type];
    if($adv){
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos    =   array_search('unknown',$arr);
            if(false !== $pos) unset($arr[$pos]);
            $ip     =   trim($arr[0]);
        }elseif (isset($_SERVER['HTTP_CLIENT_IP'])){
            $ip     =   $_SERVER['HTTP_CLIENT_IP'];
        }elseif (isset($_SERVER['REMOTE_ADDR'])){
            $ip     =   $_SERVER['REMOTE_ADDR'];
        }
    }elseif (isset($_SERVER['REMOTE_ADDR'])){
        $ip     =   $_SERVER['REMOTE_ADDR'];
    }
    // IP地址合法验证
    $long = sprintf("%u",ip2long($ip));
    $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
    return $ip[$type];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PHP限制非安全IP访问自定义函数</title>
</head>

<body>
<?php
$ip = $_GET['ip'];
$ips = $_GET['ips'];
//echo "<p>当前IP：".get_client_ip()."</p>";
if($ip){
  if(is_safe_ip($ip,$ips))
  {
    	  echo '<p>IP被禁止了！</p>';
  }
  else
  {
		  echo '<p>允许访问！</p>';
  }
}
?>
<hr />
<form id="form1" name="form1" method="get" action="is_safe_ip.php">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="120" height="30">黑、白名单IP：</td>
    <td><label>
      <input type="text" name="ips" id="ips" value="<?php echo $ips; ?>" />
      例如：127.0.0.77,127.0.0.1
    ,127.1.*.*
    </label></td>
  </tr>
  <tr>
    <td height="30">访问IP：</td>
    <td><label>
      <input type="text" name="ip" id="ip" value="<?php echo $ip; ?>" />
    例如：127.0.0.1</label></td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td><label>
      <input type="submit" name="button" id="button" value="提交" />
    </label></td>
  </tr>
</table>
</form>
</body>
</html>